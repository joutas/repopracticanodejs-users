const fs = require ('fs');

function writeUsersDataToFile (data) {

    console.log ("writeUsersDataToFile");

    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./usuarios.json",jsonUserData, "utf8",
        function (err) {

          if (err) {
            console.log (err);
          } else  {
            console.log ("Usuario persistido");
          }
        }
      )

   }

module.exports.writeUsersDataToFile = writeUsersDataToFile;
