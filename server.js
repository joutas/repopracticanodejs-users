require ('dotenv').config();


const express = require('express');
const app = express();
/* Inicializa el framwork para poder utilizar la variable*/
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}


app.use(express.json());
app.use(enableCORS);

const userController = require ('./controllers/UserController');
const authController = require ('./controllers/AuthController');
app.listen(port);
console.log ("API escuchando en el puerto "+ port);

app.get('/apitechu/v1/hello',
 function(req, res) {

   console.log("GET /apitechu/v1/hello");
   res.send({"msg": "Hola respondiendo desde API TECHU"});
 }
 );

//metodo get
 app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);

app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);

//para crear un usuario nuevo con post
app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);

app.post('/apitechu/v1/login',authController.loginUserV1);
app.post('/apitechu/v1/logout/:id',authController.logoutUserV1);

app.post('/apitechu/v2/login',authController.loginUserV2);
app.post('/apitechu/v2/logout/:id',authController.logoutUserV2);



//post para montruo
app.post('/apitechu/v1/monstruo/:p1/:p2',
 function(req, res) {

   console.log("POST /apitechu/v1/users");
   //res.sendFile('usuarios.json',{root:__dirname});
   //var users = require('./usuarios.json');
   //res.send (users);
   console.log("PArametros");
  console.log(req.params);
  console.log("Query String");
  console.log(req.query);
  console.log("Headers");
  console.log(req.headers);
  console.log("Body");
  console.log(req.body);



}
)


app.delete("/apitechu/v1/users/:id", userController.deleteUserV1)


//Ejercicio 2
  /*   users.forEach (function (element, index){

       if (element.id== req.params.id)
       {
        users.splice (index,1);
       }
     });
*/

//Ejercicio 3

/*for(var i in users){
        if (req.params.id==users[i].id)
        {
          users.splice(i,1);
          break;
        }
}

*/

//Ejercicio 4

/*var index = 0;
for (let user of users)
{
 if (req.params.id==user.id)
 {
     users.splice(index, 1);

     break;
  }
   index++;
 }
*/

//Ejercicio 5
/*var indexOfElement = users.findIndex(function(user){
  return user.id ==req.params.id;
});
users.splice (indexOfElement,1);

      console.log ("Usuario quitado de la lista");
      writeUsersDataToFile(users);
      res.send ({"msg":"Usuario borrado con exito"});
*/
