const io = require('../io');
const requestJson = require ('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechupatxi10ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;
const crypt = require('../crypt');






function loginUserV1(req, res) {

  console.log("POST /apitechu/v1/loginV1");
  var userLogged = false;

 console.log(req.body.email);
 console.log(req.body.password);
 var users = require('../usuarios.json');
for (var i = 0;i<users.length;i++)
 {
   console.log ("Entro en el for");
   if ((req.body.email == users[i].email) &&
   (req.body.password == users[i].password))
   {
     userLogged = true;
     console.log ("Login Correcto Id de Usuario "+users[i].id);
     users[i].logged = true;

     break;

   }

   else
   {

        userLogged = false;
   }

 }//fin for


  if (userLogged)
  {
  //res.send ("Login Correcto Id de Usuario "+users[i].id);
    res.send({"mensaje" : "Login correcto", "idUsuario" : users[i].id});
  }

  else
  {
   console.log ("Login Incorrecto");
   res.send({"mensaje" : "Login incorrecto"});
  }
}//Fin function



function loginUserV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}









function logoutUserV1(req, res) {

  console.log("POST /apitechu/v1/logoutV1");
  var userLoggedOut = false;


 var users = require('../usuarios.json');
for (var i = 0;i<users.length;i++)
 {
   console.log ("Entro en el for");
   if ((req.params.id == users[i].id) &&
   (users[i].logged == true ))
   {
     delete users[i].logged;
     console.log ("Borramos propiedad");
     userLoggedOut =true;
     break;

   }
}

   if (userLoggedOut)
   {
       console.log ("Logout correcto");
       res.send({"mensaje" : "Logout correcto", "idUsuario" : users[i].id});

   }

   else
   {
     console.log ("No esta logeado ");
     res.send({"mensaje" : "Logout incorrecto"});
   }

}



function logoutUserV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;
