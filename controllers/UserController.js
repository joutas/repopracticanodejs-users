const io = require('../io');
const requestJson = require ('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechupatxi10ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function getUsersV1(req, res) {

  console.log("GET /apitechu/v1/users");
  //res.sendFile('usuarios.json',{root:__dirname});
  var users = require('../usuarios.json');

  var count = req.query.$count;
  console.log ("count" +count);
  var newUsers = { };
  if  (count)
  {
     var contador  = users.length;
     console.log ("Total: "+contador);
     newUsers.count = contador;
  }

  var top = req.query.$top;
  console.log ("top"+top);

  if  (top)
    {
     var devolver = users.slice (0, top);
     newUsers.users = devolver;

   } else {
      newUsers.users = users;
   }




  //res.send (users);

  res.send (newUsers);
}



function getUsersV2(req, res) {

  console.log("GET /apitechu/v2/users");


  var httpClient = requestJson.createClient(baseMLabURL);
  console.log ("Client created");
  httpClient.get("user?"+ mLabAPIKey,

    function (err, resMLab, body) {
      var response = !err ? body : {
        "msg" :"Error obteniendo usuarios"
      }
      res.send (response);

    }

  )



}


function getUsersByIdV2 (req, res) {

   console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":'+id+'}';

  console.log ("la consulta es "+query);

   var httpClient = requestJson.createClient(baseMLabURL);


   console.log ("Client created");
   httpClient.get("user?"+ query+"&"+ mLabAPIKey,

     function (err, resMLab, body) {
       if (err) {
         var respose = {
           "msg":"Error obteniendo usuario"
         }
         res.status(500);
       }

       else {
         if (body.length >0) {
           var response = body[0];

         } else {
           var response = {
             "msg":"Usuario no encontrado"
           }
           res.status(404);
         }
       }



       res.send (response);

     }

   )

}


function createUserV1(req, res) {

  console.log("POST /apitechu/v1/users");
  //res.sendFile('usuarios.json',{root:__dirname});
  //var users = require('./usuarios.json');
  //res.send (users);
 console.log(req.body.first_name);
 console.log(req.body.last_name);
 console.log(req.body.email);


 var newUser = {


  "first_name": req.body.first_name,
  "last_name":req.body.last_name,
  "email": req.body.email


 }


 var users = require ('../usuarios.json');
 users.push(newUser);
 io.writeUsersDataToFile(users);
 console.log ("usuario añadido con exito");

 res.send("User added");
}





function createUserV2(req, res) {

  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);


  var newUser = {

   "id":req.body.id,
   "first_name": req.body.first_name,
   "last_name":req.body.last_name,
   "email": req.body.email,
   "password": crypt.hash(req.body.password)
}
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log ("Client created");


  httpClient.post("user?"+ mLabAPIKey,newUser,
  function (err, resMLab, body) {
      console.log ("usuario creado en Mlab");
      res.status(201).send ({"msg":"Usuario guardado"});

  }

)

}




function deleteUserV1(req,res) {
     console.log("DELETE /apitechu/v1/users/:id")
     console.log("Id es "+ req.params.id)

     var users = require('../usuarios.json');
//Ejercicio 1
    for (var i = 0;i<users.length;i++)
     {
       console.log ("Entro en el for");
       if (req.params.id == users[i].id)
       {
         console.log ("Entro en el if");

         break;

       }


     }
       users.splice(i,1);
      res.send ("Usuario borrado");
}


module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;

module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV2 = createUserV2;
